using System.Linq;
using System.Threading.Tasks;
using Board.Controllers;
using Board.Models;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace Auth.Tests
{
    public class AuthControllerTests
    {
        public AuthControllerTests()
        {
            _ctx = new BoardContext(
                new DbContextOptionsBuilder<BoardContext>()
                    .UseInMemoryDatabase("BoardTests")
                    .Options
            );
        }

        private readonly BoardContext _ctx;
    }
}