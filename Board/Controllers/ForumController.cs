using Board.Models;

namespace Board.Controllers
{
    public class ForumController : GenericController<Forum>
    {
        public ForumController(BoardContext ctx) : base(ctx)
        {
        }
    }
}