using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Board.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

using Id = System.String;

namespace Board.Controllers
{
    [ApiController]
    [Route("Board/[controller]")]
    public class GenericController<T> : ControllerBase where T : class
    {
        private readonly BoardContext _ctx;

        public GenericController(BoardContext ctx)
        {
            _ctx = ctx;
        }

        [HttpGet]
        [ProducesResponseType(200)]
        public async Task<ActionResult<List<T>>> Get() 
        {
            throw new NotImplementedException();
        }

        [HttpPost]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        public async Task<ActionResult<Id>> Create([FromBody] T item)
        {
            throw new NotImplementedException();
        }

        [HttpDelete]
        [ProducesResponseType(204)]
        public async Task Delete()
        {
            throw new NotImplementedException();
        }

        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public async Task<ActionResult<T>> Get(Id id)
        {
            throw new NotImplementedException();
        }

        [HttpPut("{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        public async Task<ActionResult<Category>> Update(Id id, T item)
        {
            throw new NotImplementedException();
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        public async Task Delete(Id id)
        {
            throw new NotImplementedException();
        }
    }
}