using Board.Models;

namespace Board.Controllers
{
    public class CategoryController : GenericController<Category>
    {
        public CategoryController(BoardContext ctx) : base(ctx)
        {
        }
    }
}