using Board.Models;

namespace Board.Controllers
{
    public class ThreadController : GenericController<Thread>
    {
        public ThreadController(BoardContext ctx) : base(ctx)
        {
        }
    }
}