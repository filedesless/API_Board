using Board.Models;

namespace Board.Controllers
{
    public class PostController : GenericController<Post>
    {
        public PostController(BoardContext ctx) : base(ctx)
        {
        }
    }
}