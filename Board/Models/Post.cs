using System.ComponentModel.DataAnnotations;

namespace Board.Models
{
    public class Post
    {
        public string Id { get; set; }
        
        [Required]
        public string AuthorId { get; set; }
        [Required]
        public string Content { get; set; }
        [Required]
        public string Timestamp { get; set; }

        [Required]
        public string ThreadId { get; set; }
        public Thread Thread { get; set; }
    }
}