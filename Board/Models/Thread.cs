using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Board.Models
{
    public class Thread
    {
        public string Id { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string AuthorId { get; set; }
        [Required]
        public string Op { get; set; }
        [Required]
        public string Timestamp { get; set; }

        [Required]
        public string CategoryId { get; set; }
        public Category Category { get; set; }

        public List<Post> Replies { get; set; }
    }
}