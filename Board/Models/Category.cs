using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Board.Models
{
    public class Category
    {
        public string Id { get; set; }
        [Required]
        public string Name { get; set; }

        public List<Forum> Forums { get; set; }
    }
}